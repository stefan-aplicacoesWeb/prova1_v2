/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.io.Serializable;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author stefan
 */
@ManagedBean
@RequestScoped
public class LoginBean implements Serializable {
    private String usuario;
    private String senha;
    private boolean administrador;
    private final Date date;

    @Inject
    private ApplicationContainer applicationContainer;
    
    
    public LoginBean() {
        this.date = new Date();
    }
    
    public LoginBean(LoginBean other){
        this.usuario = other.usuario;
        this.senha = other.senha;
        this.administrador = other.administrador;
        this.date = other.date;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public Date getDate() {
        return date;
    }

    public ApplicationContainer getApplicationContainer() {
        return applicationContainer;
    }

    public void setApplicationContainer(ApplicationContainer applicationContainer) {
        this.applicationContainer = applicationContainer;
    }
    
    
    public String entrarAction() {
        if(!senha.equals(usuario)){
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Acesso negado", "Acesso negado"));
            return "Acesso negado";
        }
        
        if(applicationContainer == null){
            applicationContainer = new ApplicationContainer();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Criando applicationContainer", "--"));
        }
        applicationContainer.addLogin(this);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Há " + applicationContainer.getLogins().length + " Logins", "Há " + applicationContainer.getLogins().length + " Logins"));
        if(administrador){
            return "admin";
        } else {
            return "cadastro";
        }
//        return "false";
    }
    
    
}
